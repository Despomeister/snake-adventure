
window.onload = () => {
    let G = new Game();
    G.start();     
}


class Game {

   constructor()
   {
       this.data = this.prepare();
       let snakeSpeed = 1;
       var mc = new Hammer.Manager(document.getElementById('c1'), {
           recognizers: [
               [Hammer.Swipe],
               [Hammer.Tap]
           ]
       });
       mc.on("swipe", (ev) => {
           let snake = this.data.snake;
           if(ev.direction == Hammer.DIRECTION_LEFT && snake.moveOrientation!="x")//
           {
               snake.moveOrientation = "x";
               snake.moveSpeed = -snakeSpeed;
           }
           else if (ev.direction == Hammer.DIRECTION_UP&& snake.moveOrientation!="y")
           {
               snake.moveOrientation = "y";
               snake.moveSpeed = -snakeSpeed;
           }
           else if (ev.direction == Hammer.DIRECTION_RIGHT && snake.moveOrientation!="x")
           {
               snake.moveOrientation = "x";
               snake.moveSpeed = snakeSpeed;
           }
           else if (ev.direction == Hammer.DIRECTION_DOWN && snake.moveOrientation!="y")//
           {
               snake.moveOrientation = "y";
               snake.moveSpeed = snakeSpeed;
           } 
       });

       mc.on("tap", (ev) => {
        if(this.data.startGameFlag)
        {
           this.data.startGameFlag = false;
           this.countDown(this.data);
       }
       })
       
   }

   getBoxSize() 
   {
       return 25;
   }

   start()
   {
       //Tworzenie nowej klatki
       this.tm = setInterval(() =>{
           if (!this.data.startGameFlag && !this.data.wonGameFlag && !this.data.gameOverFlag)
           {
            this.checkColission();
            this.update();
           }
           this.redraw();
       }, 150);
   }

   update()
   {
       let snake = this.data.snake;
       snake.segment.reverse().forEach((csegment, i , arr) => {
           if(snake.moveOrientation === "x") //Oreintacja pozioma
           {
               if(i == arr.length -1)   //Pierwszy segment weza
               {
                   csegment.x += snake.moveSpeed;
               }
               else
               {
                   csegment.x = snake.segment[i+1].x;
                   csegment.y = snake.segment[i+1].y;
               }
           }
           else                        //Orientacja pionowa
           {
               if(i == arr.length -1)   //Pierwszy segment weza
               {
                   csegment.y += snake.moveSpeed;
               }
               else
               {
                   csegment.x = snake.segment[i+1].x;
                   csegment.y = snake.segment[i+1].y;
               }
           }

           if(csegment.x > this.data.cvs.width / this.getBoxSize() - 1)  // Kolizja z krancami mapy x 
           {
               csegment.x = 1;
           }
           if(csegment.x < 0)
           {
               csegment.x = Math.floor(this.data.cvs.width / this.getBoxSize()) - 2;
           }

           if(csegment.y > this.data.cvs.height / this.getBoxSize() - 1)  // Kolizja z krancami mapy y 
           {
               csegment.y = 1;
           }
           if(csegment.y < 0)
           {
               csegment.y = Math.floor(this.data.cvs.height / this.getBoxSize()) - 2;
           }
       });
       snake.segment.reverse();

       //Jesli owoc nie zostal sworzony a jest wolne miejsce na mapie to stworz owoc w wolnym miejscu
       if(this.data.fruit.isGenerated == false && snake.segment.length < (this.data.cvs.width / this.getBoxSize()) * this.data.cvs.height / this.getBoxSize())
       {
           this.data.fruit = new Fruit(Math.floor(this.data.cvs.width / this.getBoxSize() - 3), Math.floor(this.data.cvs.height / this.getBoxSize() - 3), snake.segment);
       }
   }

   reset() {
       this.data.snake = new Snake(4,5);
       this.sleep(500);
   }

   checkColission()
   {
       let snake = this.data.snake;
       let fruit = this.data.fruit;

       //Zjadanie owocu
       if(snake.segment[0].x == fruit.x && snake.segment[0].y == fruit.y)
       {
           snake.segment.push({x: snake.segment[snake.segment.length - 1].x, y: snake.segment[snake.segment.length - 1].y })
           this.data.fru--;


           if (this.data.fru == 0)
           {
               this.data.openGate = true;

           }
           if(!this.data.openGate)
           {
            this.data.fruit = new Fruit(Math.floor(this.data.cvs.width / this.getBoxSize() - 3), Math.floor(this.data.cvs.height / this.getBoxSize() - 3), snake.segment);
           }
           else
           {
            this.data.fruit = new Fruit(-100, -100, snake.segment);
           }
       }

       //Zjadanie ogona weza
       var csegment;
       for(var i = 1; i < snake.segment.length; i ++){
        csegment = snake.segment[i];
           if(snake.segment[0].x == csegment.x && snake.segment[0].y == csegment.y)
           {
            this.data.lvs--;
            navigator.vibrate(100);
            if (this.data.lvs == 0) 
            {
               this.data.lvs = 3; 
               this.data.fru = 20;
               this.data.gameOverFlag = true;
            }
             
            this.reset();
           }
       }

       if (this.data.openGate)
       {
           var head = snake.segment[0];
           if (((head.x == Math.floor(window.innerWidth / this.getBoxSize())-1)) &&
           (head.y == Math.floor(window.innerHeight / (this.getBoxSize() * 2))) ||
           ((head.x == Math.floor(window.innerWidth / this.getBoxSize())-1)) &&
           (head.y == Math.floor(window.innerHeight / (this.getBoxSize() * 2)) + 1))
           {
            navigator.vibrate(100);
            this.data.lvs = 4; 
            this.data.fru = 20;

            this.data.openGate = false;
            this.data.wonGameFlag = true;
            this.data.fruit = new Fruit(Math.floor(this.data.cvs.width / this.getBoxSize() - 3), Math.floor(this.data.cvs.height / this.getBoxSize() - 3), snake.segment);
            this.reset();
           }
       }

       //Kolizja z ramka
       if(snake.segment[0].x == 0 ||
         snake.segment[0].x == Math.floor(window.innerWidth / this.getBoxSize()) - 1 || 
         snake.segment[0].y == 0 || 
         snake.segment[0].y == Math.floor(window.innerHeight / this.getBoxSize()) -2)
       {
           this.data.lvs--;
           navigator.vibrate(100);
           if (this.data.lvs == 0) 
           {
              this.data.lvs = 3; 
              this.data.fru = 20;
              this.data.gameOverFlag = true;
           }
            
           this.reset();
        }
   }

    sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

  gameOver(ctx, wdth, hght) {
      for (var i = 0; i < Math.floor(window.innerWidth / this.getBoxSize()); i++)
      {
            for (var j = 0; j < Math.floor(window.innerHeight/this.getBoxSize()); j++)
            {
                ctx.drawImage(this.data.img.br2, i * this.getBoxSize(), j * this.getBoxSize());
            }
      }
      ctx.font = "50px Arial";
      ctx.fillStyle = "#ffffff";
      ctx.fillText("GAME OVER", 
            (wdth/2-5) * this.getBoxSize(),
            (hght/2) * this.getBoxSize());
       this.data.time="3:00";
       clearInterval(this.data.refreshInterval);
       this.data.openGate = false;
  }


  wonGame(ctx, wdth, hght,text) {
    for (var i = 0; i < Math.floor(window.innerWidth / this.getBoxSize()); i++)
    {
          for (var j = 0; j < Math.floor(window.innerHeight/this.getBoxSize()); j++)
          {
              ctx.drawImage(this.data.img.br2, i * this.getBoxSize(), j * this.getBoxSize());
          }
    }
    ctx.font = "50px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText("YOU WIN", 
          (wdth/2-4) * this.getBoxSize(),
          (hght/2) * this.getBoxSize());

    
    ctx.font = "40px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText(text, 
        (wdth/2-5) * this.getBoxSize(),
        (hght/2+2) * this.getBoxSize());

    ctx.font = "40px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText("Best Time: " + this.data.bestScore, 
        (wdth/2-5) * this.getBoxSize(),
        (hght/2+4) * this.getBoxSize());

    clearInterval(this.data.refreshInterval);

}

  startGame(ctx, wdth, hght, snake) {
    for (var i = 0; i < Math.floor(window.innerWidth / this.getBoxSize()); i++)
    {
          for (var j = 0; j < Math.floor(window.innerHeight/this.getBoxSize()); j++)
          {
              ctx.drawImage(this.data.img.br1, i * this.getBoxSize(), j * this.getBoxSize());
          }
    }
    ctx.font = "45px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText("CLICK TO START", 
          (wdth/2-7) * this.getBoxSize(),
          (hght/2) * this.getBoxSize());
    this.data.fruit = new Fruit(Math.floor(this.data.cvs.width / this.getBoxSize() - 3), Math.floor(this.data.cvs.height / this.getBoxSize() - 3), snake.segment);
}

countDown(data)
{
    var counter = 0;
    var timeLeft = 180;

    function converSeconds(s)
    {
        var min = Math.floor(s / 60);
        var sec = s % 60;
        if(sec > 9)
        return min + ":" + sec;
        else
        return min + ":0" + sec;
    }

    if(this.data.time == "0:00")
    {
        data.gameOverFlag = true;
        clearInterval(data.refreshInterval);
    }

    function timeIt()
    {
        counter++;
        data.time = converSeconds(timeLeft - counter);
        if(counter == 180)
        {
            data.gameOverFlag = true;
            clearInterval(data.refreshInterval);
            navigator.vibrate(100);
            data.lvs = 3; 
            data.fru = 25;
            data.snake = new Snake(4,5);
        }
    }
    data.refreshInterval = setInterval(timeIt, 1000);


}

   redraw()
   {
           let context = this.data.ctx;
           let img = this.data.img;
           let fruit = this.data.fruit;
           let snake = this.data.snake;
       
           context.drawImage(img.bg, 0, 0);
           let wdth = Math.floor(window.innerWidth / this.getBoxSize()); //38.4
           let hght = Math.floor(window.innerHeight / this.getBoxSize()); //21.6
           
           for(var i = 0; i < wdth+1; i++)
           {
               for(var j = 0; j < hght+1; j++)
               {   
                   if((i == 0 || j == 0 || i >= wdth-1 || j >= hght-2))
                   {
                        if (!this.data.openGate)
                            context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * j, this.getBoxSize(), this.getBoxSize());
                        else
                            if (((i >= wdth - 1) && 
                            (j == Math.floor(hght/2) ||
                            j == Math.floor(hght/2) + 1)))
                                continue;
                            context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * j, this.getBoxSize(), this.getBoxSize());
                   }

               }
           }

           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //Rysowanie liczby owocow do zdobycia
           context.font = "50px Arial";
           context.fillStyle = "#ffffff";

           if(this.data.fru < 10)
           {
            context.fillText(this.data.fru, 
                (wdth-2) * this.getBoxSize(),
                (hght) * this.getBoxSize());

                context.drawImage(img.fr, fruit.type * this.getBoxSize(), 0,
                 this.getBoxSize(), this.getBoxSize(), (wdth -3)* this.getBoxSize(),
                  (hght - 1) * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());

           }
           else{
            context.fillText(this.data.fru,
                (wdth-3) * this.getBoxSize(),
                (hght) * this.getBoxSize());

                context.drawImage(img.fr, fruit.type * this.getBoxSize(), 0,
                 this.getBoxSize(), this.getBoxSize(), (wdth -4)* this.getBoxSize(),
                  (hght - 1) * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());
           }
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //Rysowanie
           context.font = "50px Arial";
           context.fillStyle = "#ffffff";

            context.fillText(this.data.time, 
                ((wdth-6)/2 + 3) * this.getBoxSize(),
                (hght) * this.getBoxSize());

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Rysowanie serc
           for(var i = 0; i < this.data.lvs; i ++)
           {
                context.drawImage(img.sr,this.getBoxSize() + this.getBoxSize() * i * this.data.img.sr.height/this.getBoxSize(),(hght-2) * this.getBoxSize());
           }
           
           //Rysowanie owocu
           context.drawImage(img.fr, fruit.type * this.getBoxSize(), 0, this.getBoxSize(), this.getBoxSize(), fruit.x * this.getBoxSize(), fruit.y * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());

           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //Rysowanie weza
           snake.segment.forEach((csegment, i, arr) => {
               if(i == 0 )
               {
                   //Glowa weza
                   let nextSegment = snake.segment[i+1];
                   if(csegment.x - nextSegment.x == 1 || csegment.x - nextSegment.x < -1)
                   {
                       context.drawImage(img.sn, 100, this.getBoxSize(), this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   } 
                   else if(csegment.x - nextSegment.x == -1 || csegment.x - nextSegment.x > 1)
                   {
                       context.drawImage(img.sn, 100, 0, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   }
                   else if(csegment.y - nextSegment.y == 1 || csegment.y - nextSegment.y < -1)
                   {
                       context.drawImage(img.sn, 75, 0, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   }
                   else if(csegment.y - nextSegment.y == -1 || csegment.y - nextSegment.y > 1)
                   {
                       context.drawImage(img.sn, 75, this.getBoxSize(), this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   }
               }
               //Ogon weza
               else if(i == arr.length - 1)
               {
                   let previousSegment = snake.segment[i-1];
                   //
                   if(csegment.x - previousSegment.x == 1  || csegment.x - previousSegment.x < -1)
                   {
                       context.drawImage(img.sn, this.getBoxSize(), 50, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   } 
                   //
                   else if(csegment.x - previousSegment.x == -1  || csegment.x - previousSegment.x > 1)
                   {
                       context.drawImage(img.sn, 75, 50, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   }
                   //
                   else if(csegment.y - previousSegment.y == 1  || csegment.y - previousSegment.y < -1)
                   {
                       context.drawImage(img.sn, 50, 50, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   }
                   //
                   else if(csegment.y - previousSegment.y == -1  || csegment.y - previousSegment.y > 1)
                   {
                       context.drawImage(img.sn, 0, 50, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   }
               }
               else
               {
                   let nextSegment = snake.segment[i+1];
                   let previousSegment = snake.segment[i-1];

                   //Rysowanie tlowia weza poziomo
                   if((csegment.x - nextSegment.x == 1 && csegment.x - previousSegment.x == -1) ||
                   (csegment.x - previousSegment.x == 1 && csegment.x - nextSegment.x == -1) ||
                   (csegment.x - nextSegment.x <= -1 && csegment.x - previousSegment.x <= -1) ||
                   (csegment.x - nextSegment.x >= 1 && csegment.x - previousSegment.x >= 1))
                   {
                       context.drawImage(img.sn, this.getBoxSize(), this.getBoxSize(), this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                   }
                   //Rysowanie tlowia weza pionowo
                   else if ((csegment.y - nextSegment.y== 1 && csegment.y - previousSegment.y) == -1 ||
                       (csegment.y - previousSegment.y == 1 && csegment.y - nextSegment.y == -1) ||
                       (csegment.y - nextSegment.y <= -1 && csegment.y - previousSegment.y <= -1) ||
                       (csegment.y - nextSegment.y >= 1 && csegment.y - previousSegment.y >= 1)) 
                       {
                           context.drawImage(img.sn, this.getBoxSize(), 0, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                       }
                   //Rysowanie tlowia weza podczas skretu
                   else if(csegment.x - nextSegment.x == 1 && csegment.y  - previousSegment.y == 1 ||
                       csegment.x - previousSegment.x == 1 && csegment.y  - nextSegment.y == 1 ||
                       csegment.x - nextSegment.x == 1 && csegment.y  - previousSegment.y < -1 ||
                       csegment.x - previousSegment.x == 1 && csegment.y  - nextSegment.y < -1 ||
                       csegment.x - nextSegment.x < -1 && csegment.y  - previousSegment.y == 1 ||
                       csegment.x - previousSegment.x < -1 && csegment.y  - nextSegment.y == 1)
                       {
                           context.drawImage(img.sn, 50, this.getBoxSize(), this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                       }
                       else if(csegment.x - nextSegment.x == 1 && csegment.y  - previousSegment.y == -1 ||
                           csegment.x - previousSegment.x == 1 && csegment.y  - nextSegment.y == -1 ||
                           csegment.x - nextSegment.x == 1 && csegment.y  - previousSegment.y > 1 ||
                           csegment.x - previousSegment.x == 1 && csegment.y  - nextSegment.y > 1 ||
                           csegment.x - nextSegment.x < -1 && csegment.y  - previousSegment.y == -1 ||
                           csegment.x - previousSegment.x < -1 && csegment.y  - nextSegment.y == -1)
                           {
                               context.drawImage(img.sn, 50, 0, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                           }
                           else if(csegment.x - nextSegment.x == -1 && csegment.y  - previousSegment.y == 1 ||
                               csegment.x - previousSegment.x == -1 && csegment.y  - nextSegment.y == 1 ||
                               csegment.x - nextSegment.x == -1 && csegment.y  - previousSegment.y < -1 ||
                               csegment.x - previousSegment.x == -1 && csegment.y  - nextSegment.y < -1 ||
                               csegment.x - nextSegment.x > 1 && csegment.y  - previousSegment.y == 1 ||
                               csegment.x - previousSegment.x > 1 && csegment.y  - nextSegment.y == 1)
                               {
                                   context.drawImage(img.sn, 0, this.getBoxSize(), this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                               }
                               else if(csegment.x - nextSegment.x == -1 && csegment.y  - previousSegment.y == -1 ||
                                   csegment.x - previousSegment.x == -1 && csegment.y  - nextSegment.y == -1 ||
                                   csegment.x - nextSegment.x == -1 && csegment.y  - previousSegment.y > 1 ||
                                   csegment.x - previousSegment.x == -1 && csegment.y  - nextSegment.y > 1 ||
                                   csegment.x - nextSegment.x > 1 && csegment.y  - previousSegment.y == -1 ||
                                   csegment.x - previousSegment.x > 1 && csegment.y  - nextSegment.y == -1)
                                   {
                                       context.drawImage(img.sn, 0, 0, this.getBoxSize(), this.getBoxSize(), csegment.x * this.getBoxSize(), csegment.y * this.getBoxSize(), this.getBoxSize() ,this.getBoxSize());
                                   }
               }
           })
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //Przegrana
            if (this.data.gameOverFlag)
            {
                if (this.data.inFlag)
                {
                    this.data.milis = Date.now();
                    this.data.inFlag = false;
                }
                else if (this.data.milis + 3000 < Date.now())
                {
                    this.data.gameOverFlag = false;
                    this.data.startGameFlag = true;
                    this.data.inFlag = true;
                }
                
                this.gameOver(context, wdth, hght);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Wygrana
            var text = "Your Time: " + this.data.time;
            if (this.data.wonGameFlag)
            {
                if (this.data.inFlag)
                {
                    this.data.milis = Date.now();
                    this.data.inFlag = false;
                    console.log(this.data.time);

                }
                else if (this.data.milis + 3000 < Date.now())
                {
                    this.data.endTime = this.data.time;
                    //Firebase Add
                    var db = firebase.firestore();
                    db.collection("Results").add({
                        Result: this.data.endTime
                    })

                    this.data.wonGameFlag = false;
                    this.data.startGameFlag = true;
                    this.data.inFlag = true;
                    this.data.time="3:00";

                }
                
                this.wonGame(context, wdth, hght,text);
                 
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Start Gry
            if (this.data.startGameFlag)
            {
              this.startGame(context, wdth, hght, snake);
            }
   }

   prepare()
   {

    //FIREBASE
    var config = {
        projectId: "snakeadventure-f93e6" ,
        apiKey: "AIzaSyAxp82qHndSWLkqLVrT3nsBwhGZHZZaCqY",
        databaseURL: "https://snakeadventure-f93e6.firebaseio.com",
        storageBucket: "snakeadventure-f93e6.appspot.com",
    };
    firebase.initializeApp(config);

                    var db = firebase.firestore();
                    var bestScore;
                    db.collection("Results").orderBy("Result","desc").limit(1).get().then((querySnapshot) =>{
                        querySnapshot.forEach(doc => {
                            this.data.bestScore = doc.data().Result;
                        });
                    });

       let backgroundIMG = new Image();
       backgroundIMG.src = "img/tlo.png";

       let _canvas = document.getElementById("c1");
       _canvas.width= window.innerWidth;
       _canvas.height=window.innerHeight;
       let _context = _canvas.getContext("2d");

       
       backgroundIMG.addEventListener("load", () => {
           backgroundIMG = this;
       })

       let snakeIMG = new Image();
       snakeIMG.src = "img/snake.png";
       snakeIMG.addEventListener("load", () => {
           snakeIMG = this;
       })

       let fruitIMG = new Image();
       fruitIMG.src = "img/fruits.png";
       fruitIMG.addEventListener("load", () => {
           fruitIMG = this;
       })

       let firstBrickIMG = new Image();
       firstBrickIMG.src = "img/kostka1.png";
       firstBrickIMG.addEventListener("load", () => {
           firstBrickIMG = this;
       })

       let secondBrickIMG = new Image();
       secondBrickIMG.src = "img/kostka2.png";
       secondBrickIMG.addEventListener("load", () => {
           secondBrickIMG = this;
       })

       let heartIMG = new Image();
       heartIMG.src = "img/heart.png";
       heartIMG.addEventListener("load", () => {
        heartIMG = this;
       })
       
       let _snake = new Snake(4,5);
       // let _fruit = new Fruit(_canvas.width / this.getBoxSize(), _canvas.height / this.getBoxSize(), _snake.segment);
       let _fruit = new Fruit(Math.floor(_canvas.width / this.getBoxSize() - 3), Math.floor(_canvas.height / this.getBoxSize() - 3), _snake.segment);

       

       let data = {
           openGate: false,
           gameOverFlag: false,
           wonGameFlag: false,
           startGameFlag: true,
           refreshInterval: null,
           time: "3:00",
           bestScore: "",
           inFlag: true,
           milis: 0,
           lvs: 3,
           fru: 20,
           cvs: _canvas,
           ctx: _context,
           img: {
               bg: backgroundIMG,
               sn: snakeIMG,
               fr: fruitIMG,
               br1: firstBrickIMG,
               br2: secondBrickIMG, 
               sr: heartIMG
           },
           snake: _snake,
           fruit: _fruit
       }

       return data;
   }
}

class Snake {
   constructor(x, y){
       this.segment = [];
       for(let i = 0; i < 4; i++)
       {
           this.segment[i] = {x: x-i, y: y};
       }
       this.moveOrientation = "x";
       this.moveSpeed = 1;
   }
}

class Fruit {
constructor(fx, fy, segment)
{
   this.isGenerated= false;
   this.genPosition(fx, fy, segment);
   this.type = Math.floor(Math.random() * 3); 
}

genPosition(fx,fy,snakeSegment)
{
   if(snakeSegment.length < (fx+1) * (fy+1))
   {
       //Losowanie pozycji owocu
       let position = { x: 1 + Math.floor(Math.random() * fx), y: 1 + Math.floor(Math.random() * fy)}
       let fruitCollision = false;

       snakeSegment.some((csegment) => {
           if(csegment.x == position.x && csegment.y == position.y)
           {
               fruitCollision = true;
               return true;
           }
       });

       if(fruitCollision == true)
       {
           this.genPosition(fx, fy, snakeSegment)
       } else {
           this.x = position.x;
           this.y = position.y;
           this.isGenerated = true;
       }
   }
   else
   {
       this.g = false;
   }
}
}
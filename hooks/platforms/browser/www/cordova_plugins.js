cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-vibration/src/browser/Vibration.js",
        "id": "cordova-plugin-vibration.Vibration",
        "pluginId": "cordova-plugin-vibration",
        "merges": [
            "navigator"
        ]
    },
    {
        "file": "plugins/cordova-plugin-vibration/www/vibration.js",
        "id": "cordova-plugin-vibration.notification",
        "pluginId": "cordova-plugin-vibration",
        "merges": [
            "navigator"
        ]
    },
    {
        "file": "plugins/cordova-plugin-firestore/www/browser/firestore.js",
        "id": "cordova-plugin-firestore.Firestore",
        "pluginId": "cordova-plugin-firestore",
        "clobbers": [
            "Firestore"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-plugin-vibration": "3.1.1",
    "cordova-plugin-firestore": "3.0.0",
    "cordova-plugin-enable-multidex": "0.2.0"
}
// BOTTOM OF METADATA
});
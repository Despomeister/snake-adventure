window.onload = () => {
    let G = new Game();
    G.start();     
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Klasa Gra
class Game {

   constructor()
   {
       this.data = this.prepare();
       let snakeSpeed = 1;

       //Swipe
       var mc = new Hammer.Manager(document.getElementById('c1'), {
           recognizers: [
               [Hammer.Swipe],
               [Hammer.Tap]
           ]
       });
       mc.on("swipe", (ev) => {
           let snake = this.data.snake;
           if(ev.direction == Hammer.DIRECTION_LEFT && snake.moveOrientation!="x")//
           {
               snake.moveOrientation = "x";
               snake.moveSpeed = -snakeSpeed;
           }
           else if (ev.direction == Hammer.DIRECTION_UP&& snake.moveOrientation!="y")
           {
               snake.moveOrientation = "y";
               snake.moveSpeed = -snakeSpeed;
           }
           else if (ev.direction == Hammer.DIRECTION_RIGHT && snake.moveOrientation!="x")
           {
               snake.moveOrientation = "x";
               snake.moveSpeed = snakeSpeed;
           }
           else if (ev.direction == Hammer.DIRECTION_DOWN && snake.moveOrientation!="y")//
           {
               snake.moveOrientation = "y";
               snake.moveSpeed = snakeSpeed;
           } 
       });

       mc.on("tap", (ev) => {
        if(this.data.startGameFlag)
        {
           this.data.startGameFlag = false;
           this.countDown(this.data);
       }
       })
       
   }
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   //Rozmiar fragmentu
   getBoxSize() 
   {
       return 25;
   }
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   //Start Gry
   start()
   {
       //Tworzenie nowej klatki
       this.tm = setInterval(() =>{
           if (!this.data.startGameFlag && !this.data.wonGameFlag && !this.data.gameOverFlag)
           {
            this.checkColission();
            this.update();
           }
           this.repaint();
       }, 200);
   }
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   //Zmiana pozycji weza
   update()
   {
       let snake = this.data.snake;
       snake.fragment.reverse().forEach((currentFragment, i , arr) => {
           if(snake.moveOrientation === "x")
           {
               //Glowa weza
               if(i == arr.length - 1)  
               {
                   currentFragment.x += snake.moveSpeed;
               }
               //Reszta weza
               else
               {
                   currentFragment.x = snake.fragment[i+1].x;
                   currentFragment.y = snake.fragment[i+1].y;
               }
           }
           else if(snake.moveOrientation === "y")
           {
               //Glowa weza
               if(i == arr.length - 1)  
               {
                   currentFragment.y += snake.moveSpeed;
               }
               //Reszta weza
               else
               {
                   currentFragment.x = snake.fragment[i+1].x;
                   currentFragment.y = snake.fragment[i+1].y;
               }
           }
       });
       snake.fragment.reverse();
   }
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   //Ustawienie weza na pozycje startowa
   reset() {
       this.data.snake = new Snake(2,3);
       this.sleep(500);
   }
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   //Sprawdzanie kolizji weza z obiektami
   checkColission()
   {
       let snake = this.data.snake;
       let fruit = this.data.fruit;
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       //Zjadanie owocu
       if(snake.fragment[0].x == fruit.x && snake.fragment[0].y == fruit.y)
       {
           this.data.fru--;
           snake.fragment.push({x: snake.fragment[snake.fragment.length - 1].x, y: snake.fragment[snake.fragment.length - 1].y })

           if (this.data.fru == 0)
           {
               this.data.openGate = true;

           }

           if(!this.data.openGate)
           {
            this.data.fruit = new Fruit(Math.floor(this.data.cvs.width / this.getBoxSize() - 3), Math.floor(this.data.cvs.height / this.getBoxSize() - 3), snake.fragment);
           }
           else
           {
            this.data.fruit = new Fruit(-100, -100, snake.fragment);
           }
       }
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       //Zjadanie ogona weza
       var currentFragment;
       for(var i = 1; i < snake.fragment.length; i ++){
        currentFragment = snake.fragment[i];
           if(snake.fragment[0].x == currentFragment.x && snake.fragment[0].y == currentFragment.y)
           {
            this.data.lvs--;
            navigator.vibrate(100);
            if (this.data.lvs == 0) 
            {
               this.data.lvs = 3; 
               this.data.fru = 10;
               this.data.gameOverFlag = true;
            }
             
            this.reset();
           }
       }
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       //Kolizja weza z brama
       if (this.data.openGate)
       {
           var head = snake.fragment[0];
           if (((head.x == Math.floor(window.innerWidth / this.getBoxSize())-1)) &&
           (head.y == Math.floor(window.innerHeight / (this.getBoxSize() * 2))) ||
           ((head.x == Math.floor(window.innerWidth / this.getBoxSize())-1)) &&
           (head.y == Math.floor(window.innerHeight / (this.getBoxSize() * 2)) + 1))
           {
            navigator.vibrate(100);
            this.data.lvs = 4; 
            this.data.fru = 10;

            this.data.openGate = false;
            this.data.wonGameFlag = true;
            this.data.fruit = new Fruit(Math.floor(this.data.cvs.width / this.getBoxSize() - 3), Math.floor(this.data.cvs.height / this.getBoxSize() - 3), snake.fragment);
            this.reset();
           }
       }
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       //Kolizja z ramka
       if(snake.fragment[0].x == 0 ||
         snake.fragment[0].x == Math.floor(window.innerWidth / this.getBoxSize()) - 1 || 
         snake.fragment[0].y == 0 || 
         snake.fragment[0].y == Math.floor(window.innerHeight / this.getBoxSize()) -2)
       {
           this.data.lvs--;
           navigator.vibrate(100);
           if (this.data.lvs == 0) 
           {
              this.data.lvs = 3; 
              this.data.fru = 10;
              this.data.gameOverFlag = true;
           }
            
           this.reset();
        }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Kolizja ze sciana
    let wdth = Math.floor(window.innerWidth / this.getBoxSize());
    let hght = Math.floor(window.innerHeight / this.getBoxSize());
    if(snake.fragment[0].x >= Math.floor(wdth/4) && snake.fragment[0].x < Math.floor((wdth*3)/4))
        {
            if((snake.fragment[0].x == Math.floor((wdth*3)/4 - 1)  && snake.fragment[0].y >= Math.floor(hght/4) + 1 && snake.fragment[0].y <= Math.floor((hght*3)/4 - 1)) ||
            (snake.fragment[0].x == Math.floor(wdth/2) &&  snake.fragment[0].y >= Math.floor(hght/4) - 1 && snake.fragment[0].y <= Math.floor(hght/2 - 1))  ||
            (snake.fragment[0].x == Math.floor(wdth/4)+2 && snake.fragment[0].y >= Math.floor(hght/2) - 2 && snake.fragment[0].y <= Math.floor((hght*3)/4))||
            (snake.fragment[0].x >= Math.floor(wdth/4) && snake.fragment[0].x < Math.floor((wdth*3)/4) && snake.fragment[0].y == Math.floor(hght/2)))
            {
                this.data.lvs--;
                navigator.vibrate(100);
                if (this.data.lvs == 0) 
                {
                    this.data.lvs = 3; 
                    this.data.fru = 10;
                    this.data.gameOverFlag = true;
                }
                
            this.reset();
            }
        }

   }

   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   //Uspienie weza
    sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //Koniec gry - Przegrana
  gameOver(ctx, wdth, hght) {
      for (var i = 0; i < Math.floor(window.innerWidth / this.getBoxSize()); i++)
      {
            for (var j = 0; j < Math.floor(window.innerHeight/this.getBoxSize()); j++)
            {
                ctx.drawImage(this.data.img.br2, i * this.getBoxSize(), j * this.getBoxSize());
            }
      }
      ctx.font = "50px Arial";
      ctx.fillStyle = "#ffffff";
      ctx.fillText("GAME OVER", 
            (wdth/2-5) * this.getBoxSize(),
            (hght/2) * this.getBoxSize());
       this.data.time="3:00";
       clearInterval(this.data.refreshInterval);
       this.data.openGate = false;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //Koniec gry - Wygrana
  wonGame(ctx, wdth, hght,text) {
    for (var i = 0; i < Math.floor(window.innerWidth / this.getBoxSize()); i++)
    {
          for (var j = 0; j < Math.floor(window.innerHeight/this.getBoxSize()); j++)
          {
              ctx.drawImage(this.data.img.br2, i * this.getBoxSize(), j * this.getBoxSize());
          }
    }
    ctx.font = "50px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText("YOU WIN", 
          (wdth/2-4) * this.getBoxSize(),
          (hght/2) * this.getBoxSize());

    
    ctx.font = "40px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText(text, 
        (wdth/2-5) * this.getBoxSize(),
        (hght/2+2) * this.getBoxSize());

    ctx.font = "40px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText("Best Time: " + this.data.bestScore, 
        (wdth/2-5) * this.getBoxSize(),
        (hght/2+4) * this.getBoxSize());

    clearInterval(this.data.refreshInterval);

}

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //Start Gry / Restart
  startGame(ctx, wdth, hght, snake) {
    for (var i = 0; i < Math.floor(window.innerWidth / this.getBoxSize()); i++)
    {
          for (var j = 0; j < Math.floor(window.innerHeight/this.getBoxSize()); j++)
          {
              ctx.drawImage(this.data.img.br2, i * this.getBoxSize(), j * this.getBoxSize());
          }
    }
    ctx.font = "45px Arial";
    ctx.fillStyle = "#ffffff";
    ctx.fillText("CLICK TO START", 
          (wdth/2-7) * this.getBoxSize(),
          (hght/2) * this.getBoxSize());
    this.data.fruit = new Fruit(Math.floor(this.data.cvs.width / this.getBoxSize() - 3), Math.floor(this.data.cvs.height / this.getBoxSize() - 3), snake.fragment);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Odliczanie zegara
countDown(data)
{
    var counter = 0;
    var timeLeft = 180;

    function converSeconds(s)
    {
        var min = Math.floor(s / 60);
        var sec = s % 60;
        if(sec > 9)
        return min + ":" + sec;
        else
        return min + ":0" + sec;
    }

    if(this.data.time == "0:00")
    {
        data.gameOverFlag = true;
        clearInterval(data.refreshInterval);
    }

    function timeIt()
    {
        counter++;
        data.time = converSeconds(timeLeft - counter);
        if(counter == 180)
        {
            data.gameOverFlag = true;
            clearInterval(data.refreshInterval);
            navigator.vibrate(100);
            data.lvs = 3; 
            data.fru = 25;
            data.snake = new Snake(2,3);
        }
    }
    data.refreshInterval = setInterval(timeIt, 1000);


}
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Rysowanie kolejnej klatki
        repaint()
        {
           let context = this.data.ctx;
           let img = this.data.img;
           let fruit = this.data.fruit;
           let snake = this.data.snake;
       
           context.drawImage(img.bg, 0, 0);
           let wdth = Math.floor(window.innerWidth / this.getBoxSize()); //38.4
           let hght = Math.floor(window.innerHeight / this.getBoxSize()); //21.6

           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           //Rysowanie Ramki
           for(var i = 0; i < wdth+1; i++)
           {
               for(var j = 0; j < hght+1; j++)
               {   
                   if((i == 0 || j == 0 || i >= wdth-1 || j >= hght-2))
                   {
                        if (!this.data.openGate)
                            context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * j, this.getBoxSize(), this.getBoxSize());
                        else
                            if (((i >= wdth - 1) && 
                            (j == Math.floor(hght/2) ||
                            j == Math.floor(hght/2) + 1)))
                                continue;
                            context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * j, this.getBoxSize(), this.getBoxSize());
                   }

               }
           }

           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           //Rysowanie Scian

           for(var i = Math.floor(wdth/4); i < Math.floor((wdth*3)/4); i++)
           {
                        context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * Math.floor(hght/2), this.getBoxSize(), this.getBoxSize());

                        if(i == Math.floor((wdth*3)/4 - 1))
                        {
                            for(var j = Math.floor(hght/4) + 1; j < Math.floor((hght*3)/4); j++)
                            {
                                context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * j, this.getBoxSize(), this.getBoxSize());
                            }
                        }

                        if(i == Math.floor(wdth/2))
                        {
                            for(var j = Math.floor(hght/4) - 1; j < Math.floor(hght/2); j++)
                            {
                                context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * j, this.getBoxSize(), this.getBoxSize());
                            }
                        }

                        if(i == Math.floor(wdth/4)+2)
                        {
                            for(var j = Math.floor(hght/2) - 2; j < Math.floor((hght*3)/4 + 1); j++)
                            {
                                context.drawImage(img.br1,this.getBoxSize() * i,this.getBoxSize() * j, this.getBoxSize(), this.getBoxSize());
                            }
                        }
           }

           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           //Rysowanie liczby owocow do zdobycia
           context.font = "50px Arial";
           context.fillStyle = "#FFFFFF";
           context.strokeStyle = 'black';

           if(this.data.fru < 10)
           {
            context.fillText(this.data.fru, 
                (wdth-2) * this.getBoxSize(),
                (hght) * this.getBoxSize());
            context.drawImage(img.fr,(wdth -3)* this.getBoxSize(),(hght - 1) * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());
            

            context.strokeText(this.data.fru, 
            (wdth-2) * this.getBoxSize(),
            (hght) * this.getBoxSize());
            context.drawImage(img.fr,(wdth -3)* this.getBoxSize(),(hght - 1) * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());
    
           }
           else{
            context.fillText(this.data.fru,
                (wdth-3) * this.getBoxSize(),
                (hght) * this.getBoxSize());
                context.drawImage(img.fr,(wdth -4)* this.getBoxSize(),(hght - 1) * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());

            context.strokeText(this.data.fru,
                (wdth-3) * this.getBoxSize(),
                (hght) * this.getBoxSize());
                context.drawImage(img.fr,(wdth -4)* this.getBoxSize(),(hght - 1) * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());
    
           }
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           //Rysowanie czasu
           context.font = "50px Arial";
           context.fillStyle = "#FFFFFF";
           context.strokeStyle = 'black';

            context.fillText(this.data.time, 
                ((wdth-6)/2 + 3) * this.getBoxSize(),
                (hght) * this.getBoxSize());

                context.linewidth = 10;
            context.strokeText(this.data.time, 
                ((wdth-6)/2 + 3) * this.getBoxSize(),
                (hght) * this.getBoxSize());

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //Rysowanie serc
           for(var i = 0; i < this.data.lvs; i ++)
           {
                context.drawImage(img.sr,this.getBoxSize() + this.getBoxSize() * i * this.data.img.sr.height/this.getBoxSize(),(hght-2) * this.getBoxSize());
           }
           
           //Rysowanie owocu
           context.drawImage(img.fr, fruit.x * this.getBoxSize(), fruit.y * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());

           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           //Rysowanie weza
           snake.fragment.forEach((currentFragment, i, arr) => {
               if(snake.fragment[0].x == currentFragment.x && snake.fragment[0].y == currentFragment.y)
               {
                    context.drawImage(img.hd,currentFragment.x * this.getBoxSize(),currentFragment.y * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());
               }
               else
               {
                    context.drawImage(img.bd,currentFragment.x * this.getBoxSize(),currentFragment.y * this.getBoxSize(), this.getBoxSize(), this.getBoxSize());
               }
           })
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           //Przegrana
            if (this.data.gameOverFlag)
            {
                if (this.data.inFlag)
                {
                    this.data.milis = Date.now();
                    this.data.inFlag = false;
                }
                else if (this.data.milis + 3000 < Date.now())
                {
                    this.data.gameOverFlag = false;
                    this.data.startGameFlag = true;
                    this.data.inFlag = true;
                }
                
                this.gameOver(context, wdth, hght);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //Wygrana
            var text = "Your Time: " + this.data.time;
            if (this.data.wonGameFlag)
            {
                if (this.data.inFlag)
                {
                    this.data.milis = Date.now();
                    this.data.inFlag = false;
                    console.log(this.data.time);

                }
                else if (this.data.milis + 3000 < Date.now())
                {
                    this.data.endTime = this.data.time;
                    //Firebase Add
                    var db = firebase.firestore();
                    db.collection("Results").add({
                        Result: this.data.endTime
                    })

                    this.data.wonGameFlag = false;
                    this.data.startGameFlag = true;
                    this.data.inFlag = true;
                    this.data.time="3:00";

                }
                
                this.wonGame(context, wdth, hght,text);
                 
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //Start Gry
            if (this.data.startGameFlag)
            {
              this.startGame(context, wdth, hght, snake);
            }
   }

   //Tworzenie bazowych danych
   prepare()
   {

    //FIREBASE
    var config = {
        projectId: "snakeadventure-f93e6" ,
        apiKey: "AIzaSyAxp82qHndSWLkqLVrT3nsBwhGZHZZaCqY",
        databaseURL: "https://snakeadventure-f93e6.firebaseio.com",
        storageBucket: "snakeadventure-f93e6.appspot.com",
    };
    firebase.initializeApp(config);

                    var db = firebase.firestore();
                    var bestScore;
                    db.collection("Results").orderBy("Result","desc").limit(1).get().then((querySnapshot) =>{
                        querySnapshot.forEach(doc => {
                            this.data.bestScore = doc.data().Result;
                        });
                    });

       let backgroundIMG = new Image();
       backgroundIMG.src = "img/tlo4.jpg";

       let _canvas = document.getElementById("c1");
       _canvas.width= window.innerWidth;
       _canvas.height=window.innerHeight;
       let _context = _canvas.getContext("2d");

       
       backgroundIMG.addEventListener("load", () => {
           backgroundIMG = this;
       })

       let snakeIMG = new Image();
       snakeIMG.src = "img/snake.png";
       snakeIMG.addEventListener("load", () => {
           snakeIMG = this;
       })

       let fruitIMG = new Image();
       fruitIMG.src = "img/fruit.png";
       fruitIMG.addEventListener("load", () => {
           fruitIMG = this;
       })

       let firstBrickIMG = new Image();
       firstBrickIMG.src = "img/kostka1.png";
       firstBrickIMG.addEventListener("load", () => {
           firstBrickIMG = this;
       })

       let secondBrickIMG = new Image();
       secondBrickIMG.src = "img/kostka2.png";
       secondBrickIMG.addEventListener("load", () => {
           secondBrickIMG = this;
       })

       let heartIMG = new Image();
       heartIMG.src = "img/heart.png";
       heartIMG.addEventListener("load", () => {
        heartIMG = this;
       })

       let headIMG = new Image();
       headIMG.src = "img/glowa.png";
       headIMG.addEventListener("load", () => {
        headIMG = this;
       })

       let bodyIMG = new Image();
       bodyIMG.src = "img/tulow.png";
       bodyIMG.addEventListener("load", () => {
        bodyIMG = this;
       })
       
       let _snake = new Snake(2,3);
       let _fruit = new Fruit(Math.floor(_canvas.width / this.getBoxSize() - 3), Math.floor(_canvas.height / this.getBoxSize() - 3), _snake.fragment);

       let data = {
           openGate: false,
           gameOverFlag: false,
           wonGameFlag: false,
           startGameFlag: true,
           refreshInterval: null,
           time: "3:00",
           bestScore: "",
           inFlag: true,
           milis: 0,
           lvs: 3,
           fru: 10,
           cvs: _canvas,
           ctx: _context,
           snake: _snake,
           fruit: _fruit,
           img: {
               bg: backgroundIMG,
               sn: snakeIMG,
               fr: fruitIMG,
               br1: firstBrickIMG,
               br2: secondBrickIMG, 
               sr: heartIMG,
               hd: headIMG,
               bd: bodyIMG
           }
       }

       return data;
   }
}

//Klasa weza
class Snake {
    constructor(x, y){
        this.fragment = [];
        for(let i = 0; i < 3; i++)
        {
            this.fragment[i] = {x: x-i, y: y};
        }
        this.moveOrientation = "x";
        this.moveSpeed = 1;
    }
 }
 

 //Klasa owocu
 class Fruit {
 constructor(x, y, fragment)
 {
    this.isGenerated = false;
    this.genPosition(x, y, fragment);
 }
 
 genPosition(x,y,snakeFragment)
 {
    if(snakeFragment.length >= (x+1) * (y+1))
    {
        this.isGenerated = false;
    }
    else
    {
        //Losowanie pozycji owocu
        let position_x   = 1 + Math.floor(Math.random() * x);
        let position_y   = 1 + Math.floor(Math.random() * y);
        let fruitCollisionWithSnake = false;
        let fruitCollisionWithWalls = false;
        let wdth = Math.floor(window.innerWidth / 25);
        let hght = Math.floor(window.innerHeight / 25);

        //Kolizja owoza ze sciana
        if(position_x >= Math.floor(wdth/4) && position_x <= Math.floor((wdth*3)/4))
        {
            if((position_x == Math.floor((wdth*3)/4 - 1)  && position_y >= Math.floor(hght/4) + 1 && position_y <= Math.floor((hght*3)/4 - 1)) ||
            (position_x == Math.floor(wdth/2) &&  position_y >= Math.floor(hght/4) - 1 && position_y <= Math.floor(hght/2 - 1))  ||
            (position_x == Math.floor(wdth/4)+2 && position_y >= Math.floor(hght/2) - 2 && position_y <= Math.floor((hght*3)/4))||
            (position_x >= Math.floor(wdth/4) && position_x <= Math.floor((wdth*3)/4) && position_y == Math.floor(hght/2)))
            {
                fruitCollisionWithWalls = true;
            }
        }

        //Kolizja owoca z wezem
        snakeFragment.some((currentFragment) => {
            if(currentFragment.x == position_x && currentFragment.y == position_y)
            {
                fruitCollisionWithSnake = true;
                return true;
            }
        });
 
        if(fruitCollisionWithSnake == false && fruitCollisionWithWalls == false)
        {
            this.x = position_x;
            this.y = position_y;
            this.isGenerated = true;
        } 
        else 
        {
            this.genPosition(x, y, snakeFragment)
        }
    }
 }
 }